package com.iguansystems.repository;

import com.iguansystems.model.Task;
import org.springframework.data.repository.CrudRepository;

/**
 * Developer vxudatyan
 * .Created 11/28/17
 */

public interface TaskRepository extends CrudRepository<Task,Long> {
}