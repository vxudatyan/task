package com.iguansystems.service;

import com.iguansystems.model.Task;
import com.iguansystems.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Developer vxudatyan
 * .Created 11/28/17
 */
@Service
public class TaskService {

    @Autowired
    TaskRepository taskRepository;

    public Task save(Task task){
        return taskRepository.save(task);
    }

    public Task get(Long id){
        return taskRepository.findOne(id);
    }

    public Iterable<Task> list(){
        return taskRepository.findAll();
    }

    public void delete(Long id){
       taskRepository.delete(id);
    }
}