package com.iguansystems.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Developer vxudatyan
 * .Created 11/28/17
 */

public class TaskUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(TaskUtils.class);

    public static final String INVALID_DATA = "Invalid data";
    public static final String NOT_FOUND = "Task not found";
    public static final String DELETED = "The task deleted";

    public static final String PRIORITY = "priority";
    public static final String STATUS = "status";
    public static final String ID = "id";

    public static Long parseId(String id) {
        try {
            return Long.valueOf(id);
        } catch (NumberFormatException e) {
            LOGGER.error("", e);
            return null;
        }
    }
}