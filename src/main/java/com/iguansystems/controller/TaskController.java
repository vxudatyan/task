package com.iguansystems.controller;

import com.iguansystems.model.Task;
import com.iguansystems.service.TaskService;
import com.iguansystems.utils.TaskUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

import static com.iguansystems.utils.TaskUtils.*;

/**
 * Developer vxudatyan
 * Created 11/28/17
 */

@RestController
@RequestMapping(name = "/task")
public class TaskController {
    private static final Logger LOGGER = LoggerFactory.getLogger(TaskController.class);

    @Autowired
    TaskService taskService;

    @PostMapping("/save")
    @ResponseBody
    public String save(Task task) {
        Task savedTask = taskService.save(task);
        if (savedTask == null) {
            return INVALID_DATA;
        }

        LOGGER.debug("Task with id: {} saved", task.getId());
        return savedTask.toString();
    }

    @GetMapping("/list")
    @ResponseBody
    public Iterable<Task> list() {
        return taskService.list();
    }

    @GetMapping("/get")
    @ResponseBody
    public String get(@RequestParam Map<String, String> params) {
        Long id = TaskUtils.parseId(params.get(ID));
        if (id == null) {
            return INVALID_DATA;
        }
        Task showTask = taskService.get(id);
        if (showTask == null) {
            return NOT_FOUND;
        }
        return showTask.toString();
    }

    @PutMapping("/update")
    @ResponseBody
    public String update(@RequestParam Map<String, String> params) {
        Long id = TaskUtils.parseId(params.get(ID));
        if (id == null) {
            return INVALID_DATA;
        }
        Task task = taskService.get(id);
        if (task == null) {
            return NOT_FOUND;
        }
        task.setPriority(params.get(PRIORITY));
        task.setStatus(params.get(STATUS));
        Task savedTask = taskService.save(task);
        if (savedTask == null) {
            return INVALID_DATA;
        }

        LOGGER.debug("Task with id: {} updated", id);
        return savedTask.toString();
    }

    @DeleteMapping("/delete")
    @ResponseBody
    public String delete(@RequestParam Map<String, String> params) {
        Long id = TaskUtils.parseId(params.get(ID));
        if (id == null) {
            return INVALID_DATA;
        }
        Task task = taskService.get(id);
        if (task == null) {
            return NOT_FOUND;
        }
        taskService.delete(id);
        LOGGER.debug("Task with id: {} deleted", id);
        return DELETED;
    }
}